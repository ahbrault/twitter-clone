# Prepare NGINX
FROM nginx:alpine

# Add build from cache
COPY build /usr/share/nginx/html

# Add NGINX configuration
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Start NGINX
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]