import React from 'react';
import Wrapper from "../../components/Wrapper";
import Widgets from "../../components/Widgets";

const Notifications = () => {
    const handleTabs = (index) => {
        let tab = document.querySelector(`#tab${index}`);
        let content = document.querySelector(`#tab__content${index}`);

        if (!tab.classList.contains("active")) {
            tab.classList.add("active");
            content.classList.add("active");

            if (index === 1) {
                document.getElementById("tab2").classList.remove("active");
                document.getElementById("tab__content2").classList.remove("active");
            } else {
                document.getElementById("tab1").classList.remove("active");
                document.getElementById("tab__content1").classList.remove("active");
            }
        }
    }

    return (
        <Wrapper className={"notifications"}>
            <div className={"notificationList container"}>
                <div className="container__header">
                    <h1>Notifications</h1>
                </div>
                <div className="tab__header">
                    <div className="notifications__tab active" id={"tab1"} onClick={() => handleTabs(1)}>
                        All
                    </div>
                    <div className="notifications__tab" id={"tab2"} onClick={() => handleTabs(2)}>
                        Mentions
                    </div>
                </div>
                <div className="container__content tab__content active" id={"tab__content1"}>
                    <h2>Nothing to see here — yet</h2>
                    <p>
                        From likes to Retweets and a whole lot more, this is where all the action happens.
                    </p>
                </div>
                <div className="container__content tab__content" id={"tab__content2"}>
                    <h2>Nothing to see here — yet</h2>
                    <p>
                        When someone mentions you, you’ll find it here.
                    </p>
                </div>
            </div>
            <Widgets/>
        </Wrapper>
    );
};

export default Notifications;
