import React from 'react';
import {Button} from "@material-ui/core";

const MessageList = () => {
    return (
        <div className={"messageList container"}>
            <div className="container__header">
                <h1>Messages</h1>
            </div>
            <div className="container__content">
                <h2>Send a message, get a message</h2>
                <p>Direct Messages are private conversations between you and other people on Twitter. Share Tweets,
                    media, and more!</p>
                <Button variant={"contained"} className={"buttonPrimary"}>
                    Start a conversation
                </Button>
            </div>
        </div>
    );
};

export default MessageList;
