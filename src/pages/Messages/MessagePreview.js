import React from 'react';
import {Button} from "@material-ui/core";

const MessagePreview = () => {
    return (
        <div className={"messagePreview container"}>
            <div className="container__content">
                <h2>You don’t have a message selected</h2>
                <p>Choose one from your existing messages, or start a new one.</p>
                <Button variant={"contained"} className={"buttonPrimary"}>
                    New message
                </Button>
            </div>
        </div>
    );
};

export default MessagePreview;
