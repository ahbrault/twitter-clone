import React from 'react';
import MessageList from "./MessageList";
import MessagePreview from "./MessagePreview";
import Wrapper from "../../components/Wrapper";

const Messages = () => {
    return (
        <Wrapper>
            <div className={"messages"}>
                <MessageList/>
                <MessagePreview/>
            </div>
        </Wrapper>
    );
};

export default Messages;
