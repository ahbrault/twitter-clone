import React, {useState} from 'react';
import {Avatar, Button} from "@material-ui/core";
import {ReactComponent as ImageIcon} from '../../assets/icons/home/ImageIcon.svg';
import {ReactComponent as GifIcon} from '../../assets/icons/home/GifIcon.svg';
import {ReactComponent as PollIcon} from '../../assets/icons/home/PollIcon.svg';
import {ReactComponent as EmojiIcon} from '../../assets/icons/home/EmojiIcon.svg';
import {ReactComponent as ScheduleIcon} from '../../assets/icons/home/ScheduleIcon.svg';


const TweetBox = () => {
    const [tweet, setTweet] = useState("");

    const handleChange = ({currentTarget, value}) => {
        // let button = document.querySelector(".tweetBox__button");
        setTweet(value);
        console.log(value)
    }

    const handleUpload = () => {
        let input = document.querySelector(".tweetBox__inputFile");

        input.click();
    }

    return (
        <div className={"tweetBox"}>
            <form action="">
                <div className="tweetBox__input">
                    <Avatar
                        src={"https://abs.twimg.com/sticky/default_profile_images/default_profile_400x400.png"}/>
                    <input type="text" placeholder={"What's happening ?"} value={tweet}
                           onChange={handleChange}/>
                </div>
                <div className="tweetBox__actions">
                    <div className="tweetBox__imageInput">
                        <div className="iconContainer">
                            <ImageIcon className={"customIcons"} onClick={handleUpload}/>
                        </div>
                        <input type="file" className={"tweetBox__inputFile"}/>
                    </div>
                    <div className="iconContainer">
                        <GifIcon className={"customIcons"}/>
                    </div>
                    <div className="iconContainer">
                        <PollIcon className={"customIcons"}/>
                    </div>
                    <div className="iconContainer">
                        <EmojiIcon className={"customIcons"}/>
                    </div>
                    <div className="iconContainer">
                        <ScheduleIcon className={"customIcons"}/>
                    </div>
                    <Button variant="contained" className={"tweetBox__button"}>Tweet</Button>
                </div>
            </form>
        </div>
    );
};

export default TweetBox;
