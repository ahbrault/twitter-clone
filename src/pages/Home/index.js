import React from "react";
import Feed from "./Feed";
import Widgets from "../../components/Widgets";
import Wrapper from "../../components/Wrapper";

const Home = () => {
    return (
        <Wrapper className={"home"}>
            <Feed/>
            <Widgets/>
        </Wrapper>
    );
}

export default Home;
