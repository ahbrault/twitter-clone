import React from 'react';
import {Avatar} from "@material-ui/core";
import {ReactComponent as VerifiedIcon} from '../../assets/icons/home/VerifiedIcon.svg';
import {ReactComponent as LikeIcon} from '../../assets/icons/home/LikeIcon.svg';
import {ReactComponent as RetweetIcon} from '../../assets/icons/home/RetweetIcon.svg';
import {ReactComponent as CommentIcon} from '../../assets/icons/home/CommentIcon.svg';
import {ReactComponent as ShareIcon} from '../../assets/icons/home/ShareIcon.svg';

const Post = ({displayName, username, verified, text, image, avatar}) => {
    return (
        <div className={"post"}>
            <div className="post__avatar">
                <Avatar src={avatar}/>
            </div>
            <div className="post__body">
                <div className="post__header">
                    <div className="post__headerText">
                        <h3>
                            {displayName}
                            <span className={"post__headerSpecial"}>
                                {verified &&
                                <VerifiedIcon className={"post__badge"}/>
                                }
                                {username}
                            </span>
                        </h3>
                    </div>
                    <div className="post__headerDescription">
                        <p>{text}</p>
                    </div>
                </div>
                <img src={image} alt="post"/>
            </div>
            <div className="post__footer">
                <div className="iconContainer">
                    <CommentIcon className={"customIcons comment"}/>
                </div>
                <div className="iconContainer">
                    <RetweetIcon className={"customIcons"}/>
                </div>
                <div className="iconContainer">
                    <LikeIcon className={"customIcons"}/>
                </div>
                <div className="iconContainer">
                    <ShareIcon className={"customIcons"}/>
                </div>
            </div>
        </div>
    );
};

export default Post;
