import React from 'react';
import TweetBox from "./TweetBox";
import Post from "./Post";

const Feed = () => {
    return (
        <div className="feed container">
            <div className="feed__header">
                <h1>Home</h1>
            </div>
            <TweetBox/>
            <Post
                displayName={"Aurélien Brault"}
                username={"@ahbrault"}
                verified={true}
                text={"Lorem ipsum dolor sit amet, consectetur adipisicing elit."}
                image={"https://images.unsplash.com/photo-1631458214830-506ebaabdf03?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=975&q=80"}
                avatar={"https://abs.twimg.com/sticky/default_profile_images/default_profile_400x400.png"}
            />
            <Post
                displayName={"Aurélien Brault"}
                username={"@ahbrault"}
                verified={true}
                text={"Lorem ipsum dolor sit amet, consectetur adipisicing elit."}
                image={"https://images.unsplash.com/photo-1631462608896-9984bc8b1c33?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1567&q=80"}
                avatar={"https://abs.twimg.com/sticky/default_profile_images/default_profile_400x400.png"}
            />
            <Post
                displayName={"Aurélien Brault"}
                username={"@ahbrault"}
                verified={true}
                text={"Lorem ipsum dolor sit amet, consectetur adipisicing elit."}
                image={"https://images.unsplash.com/photo-1631416884543-73535aff8fde?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=976&q=80"}
                avatar={"https://abs.twimg.com/sticky/default_profile_images/default_profile_400x400.png"}
            />
            <Post
                displayName={"Aurélien Brault"}
                username={"@ahbrault"}
                verified={true}
                text={"Lorem ipsum dolor sit amet, consectetur adipisicing elit."}
                image={"https://images.unsplash.com/photo-1631127603176-ad9e179dc6db?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=934&q=80"}
                avatar={"https://abs.twimg.com/sticky/default_profile_images/default_profile_400x400.png"}
            />
        </div>
    );
};

export default Feed;
