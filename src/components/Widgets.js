import React from 'react';
import {TwitterTweetEmbed} from "react-twitter-embed";
import {ReactComponent as SearchIcon} from '../assets/icons/SearchIcon.svg';

const Widgets = () => {
    return (
        <div className={"widgets"}>
            <div className="widgets__input">
                <SearchIcon className={"widgets__searchIcon"}/>
                <input type="text" placeholder={"Search Twitter"}/>
            </div>

            <div className="widgets__widgetContainer">
                <h2>What's happening</h2>
                <TwitterTweetEmbed tweetId={"1436364197089234945"}/>
            </div>
            <ul className={"widgets__links"}>
                <li>Terms of Service</li>
                <li>Privacy Policy</li>
                <li>Cookie Policy</li>
                <li>Ads info</li>
                <li>More</li>
                <li className={"widgets__copyright"}>© 2021 Twitter, Inc.</li>
            </ul>
        </div>
    );
};

export default Widgets;
