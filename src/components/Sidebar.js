import React from 'react';
import SidebarOption from "./SidebarOption";
import {Button} from "@material-ui/core";

// Icons
import TwitterIcon from '@material-ui/icons/Twitter';

import {ReactComponent as HomeIcon} from '../assets/icons/sidebar/HomeIcon.svg';
import {ReactComponent as HomeIconFilled} from '../assets/icons/sidebar/HomeIconFilled.svg';

import {ReactComponent as ExploreIcon} from '../assets/icons/sidebar/ExploreIcon.svg';
import {ReactComponent as ExploreIconFilled} from '../assets/icons/sidebar/ExploreIconFilled.svg';

import {ReactComponent as NotificationIcon} from '../assets/icons/sidebar/NotificationIcon.svg';
import {ReactComponent as NotificationIconFilled} from '../assets/icons/sidebar/NotificationIconFilled.svg';

import {ReactComponent as MessageIcon} from '../assets/icons/sidebar/MessageIcon.svg';
import {ReactComponent as MessageIconFilled} from '../assets/icons/sidebar/MessageIconFilled.svg';

import {ReactComponent as BookmarkIcon} from '../assets/icons/sidebar/BookmarkIcon.svg';
import {ReactComponent as BookmarkIconFilled} from '../assets/icons/sidebar/BookmarkIconFilled.svg';

import {ReactComponent as ListIcon} from '../assets/icons/sidebar/ListIcon.svg';
import {ReactComponent as ListIconFilled} from '../assets/icons/sidebar/ListIconFilled.svg';

import {ReactComponent as ProfileIcon} from '../assets/icons/sidebar/ProfileIcon.svg';
import {ReactComponent as ProfileIconFilled} from '../assets/icons/sidebar/ProfileIconFilled.svg';

import {ReactComponent as MoreCircleIcon} from '../assets/icons/sidebar/MoreCircleIcon.svg';

import {ReactComponent as CreateIcon} from '../assets/icons/sidebar/CreateIcon.svg';


const Sidebar = ({page}) => {
    return (
        <div className={"sidebar"}>
            <TwitterIcon className={"sidebar__logo"}/>

            <SidebarOption
                Icon={HomeIcon}
                Filled={HomeIconFilled}
                text={"home"}
                active={page === "/"}
            />

            <SidebarOption
                Icon={ExploreIcon}
                Filled={ExploreIconFilled}
                text={"explore"}
                active={page === "/explore"}
            />

            <SidebarOption
                Icon={NotificationIcon}
                Filled={NotificationIconFilled}
                text={"notifications"}
                active={page === "/notifications"}
            />

            <SidebarOption
                Icon={MessageIcon}
                Filled={MessageIconFilled}
                text={"messages"}
                active={page === "/messages"}
            />

            <SidebarOption
                Icon={BookmarkIcon}
                Filled={BookmarkIconFilled}
                text={"bookmarks"}
                active={page === "/bookmarks"}
            />

            <SidebarOption
                Icon={ListIcon}
                Filled={ListIconFilled}
                text={"lists"}
                active={page === "/lists"}
            />

            <SidebarOption
                Icon={ProfileIcon}
                Filled={ProfileIconFilled}
                text={"profile"}
                active={page === "/profile"}
            />

            <SidebarOption
                Icon={MoreCircleIcon}
                text={"More"}
            />

            <Button variant={"contained"} className={"sidebar__tweet"}>
                <CreateIcon className={"customIcons"}/>
                <span>
                Tweet
                </span>
            </Button>
        </div>
    );
};

export default Sidebar;
