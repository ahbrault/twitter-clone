import React from 'react';
import {Link} from "react-router-dom";

const SidebarOption = ({Icon, Filled, text, active}) => {
    return (
        <Link to={text === "home" ? "/" : `/${text}`} className={`sidebar__option ${active ? "active" : ""}`}>
            {active ?
                <Filled className={"customIcons"}/>
                :
                <Icon className={"customIcons"}/>
            }
            <h2>{text}</h2>
        </Link>
    );
};

export default SidebarOption;
