import React from 'react';
import Sidebar from "./Sidebar";
import {useHistory} from "react-router-dom";

const Wrapper = ({className, children}) => {
    const history = useHistory();
    console.log(history);

    return (
        <div className={"app"}>
            <Sidebar page={history.location.pathname}/>
            <div className={className !== undefined && className}>
                {children}
            </div>
        </div>
    );
};

export default Wrapper;
